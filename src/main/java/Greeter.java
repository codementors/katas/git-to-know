public class Greeter {

    public static void main(String[] args) {
        new Greeter().greet();
        new Repeater().repeat("42");
    }

    private void greet() {
        System.out.println("Hello World");
    }
}
